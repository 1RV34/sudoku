# The target file's name
TARGET  = Main

# Dirnames
SRCDIR    = src
INCDIR    = inc
BUILDDIR  = obj

# Extensions
SRCEXT = cpp
INCEXT = hpp
OBJEXT = o

# Flags
INC      = -I$(INCDIR) -I/usr/include -Iext/fmt/include
CXXFLAGS = -Wall -std=c++17
LIBS     = -ldl -lstdc++fs

# Magic shit
SOURCES = $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))

# Colours
INFO = \e[96m
FILENAME = \e[01;31m
RESET = \e[0m

# =====================================================
# Source: https://stackoverflow.com/a/27794283/11964828
# =====================================================

# Defauilt Make
all: $(TARGET);

# Clean everything and remake it from scratch
remake: cleaner all

# Clean only Objecs
# The - before the rm means it should not fail the make if rm fails
clean:
	@echo -e "${INFO}Cleaning ${FILENAME}objects${RESET}"
	@-rm -rv $(BUILDDIR)/*.$(OBJEXT)

# Full Clean, Objects and Binaries
# The - before the rm means it should not fail the make if rm fails
cleaner: clean
	@echo -e "${INFO}Cleaning ${FILENAME}target${RESET}${INFO} files${RESET}"
	@-rm -v $(TARGET)

# Compile
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT) $(INCDIR)/%.$(INCEXT)
	@echo -e "${INFO}Creating ${FILENAME}$@${RESET}"
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

# Link
$(TARGET): $(OBJECTS)
	@echo -e "${INFO}Creating ${FILENAME}$@${RESET}"
	$(CXX) -o $(TARGET) $(OBJECTS) $(LIBS)
