#pragma once

#include <iostream>
#include <map>

class Group;
class Sudoku;

/**
 * A square on in a sudoku puzzle
 */
class Square {
 private:
    // We declare this method a friend, so it can access our privates
    // Doesn't actually matter what visibility we put it under, but it looked weird without one
    friend std::ostream& operator<<(std::ostream&, const Sudoku&);

 protected:
    /**
     * The value of the square
     *
     * 0 means it's not found yet and not set at initialization
     */
    unsigned int value = 0;

    /**
     * The values that are still available
     *
     * A value can be available in two ways, depending on context:
     * * A given square can be a given number because restriction doesn't forbid it
     * * A given group (row, column, cluster) has yet to find a given number
     */
    std::map<unsigned char, bool> potentialValues = {
        {1, true},
        {2, true},
        {3, true},
        {4, true},
        {5, true},
        {6, true},
        {7, true},
        {8, true},
        {9, true},
    };

    /**
     * How many different numbers the square can still be
     *
     * Basically the number of `true` values within the available map
     *
     * If the value of the square is found AND set, it's set to 0!
     */
    unsigned int potentialCount = 9;

    /**
     * A pointer to the sudoku this class belongs to
     */
    Sudoku *sudoku = nullptr;

    /**
     * The groups the square belongs into
     *
     * These pointers are always bi-directional, the groups know which squares belong in them too!
     */
    Group *row = nullptr,
        *column = nullptr,
        *cluster = nullptr;

    bool resolveRequestSent = false;

 public:
    /**
     * Getter for the potentialValues
     */
    bool canBe(unsigned int number) const;

    /**
     * Set which row the square belongs into
     * 
     * @param row Pointer to the group
     */
    void setRow(Group* row);

    /**
     * Set which column the square belongs into
     * 
     * @param column Pointer to the group
     */
    void setColumn(Group* column);

    /**
     * Set which 3x3 cluster the square belongs into
     * 
     * @param column Pointer to the group
     */
    void setCluster(Group* cluster);

    /**
     * A setter for the sudoku
     */
    void setSudoku(Sudoku*);

    /**
     * Set the square's value
     *
     * If the value has been previously set already, we throw a runtime_error!
     * 
     * @param value
     */
    void setValue(unsigned int value);

    /**
     * Get the square's value
     *
     * If not found yet, it returns 0
     * 
     * @return The square's value
     */
    unsigned int getValue() const;

    /**
     * A number has been found in the group, thus no other squares can be that value
     * 
     * @param number
     */
    void numberFound(unsigned int number);

    /**
     * Getter for the potentialCount
     */
    unsigned int getPotentialCount() const;

    /**
     * Gets the X coordinate of the square
     */
    unsigned int getX() const;

    /**
     * Gets the Y coordinate of the square
     */
    unsigned int getY() const;

    /**
     * Returns an x:y string
     *
     * Uses the row and column the square belongs in
     */
    std::string getCoords() const;
};
