#pragma once

#include <array>
#include <cstddef>

class Square;
class Sudoku;

/**
 * A group of squares
 *
 * Can be either a row, column, or cluster, but since all of those contain exactly 9 squares
 * and their order doesn't technically matter, they didn't get separated into separate child classes
 */
class Group {
 protected:
    /**
     * An array of pointers to the squares that belong in this group
     *
     * NULL initiated
     */
    std::array<Square*, 9> squares = {NULL};

    /**
     * A pointer to the sudoku this class belongs to
     */
    Sudoku *sudoku = NULL;

    /**
     * One coordinate of the group
     *
     * Used for rows and columns, but not for clusters
     */
    unsigned int index = 0;

 public:
    /**
     * Adds a square to the group to a given position
     *
     * @param square   The square we want to add
     * @param position The position we want to add it to
     */
    void addSquare(Square* square, unsigned int position);

    /**
     * A setter for the sudoku
     */
    void setSudoku(Sudoku*);

    /**
     * Checks if the group contains square with a given value
     *
     * Checks value only, not potential!
     * 
     * @param  number The number to check
     * @return        A pointer to the square containing the value or NULL if no such exists
     */
    const Square* contains(unsigned int number) const;

    /**
     * Goes through the squares and sets a given number no longer potential
     * 
     * @param number The number that was found
     */
    void setFound(unsigned int number);

    /**
     * Setter for the index
     * 
     * @param index
     */
    void setIndex(unsigned int index);

    /**
     * Getter for the index
     */
    unsigned int getIndex() const;
};
