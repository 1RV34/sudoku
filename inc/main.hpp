#pragma once

/**
 * If this file doesn't exist, make will fail
 *
 * And I cannot be arsed to fix it
 */

/**
 * The typical main function, the entrypoint of the code
 *
 * Returns 0 if everything goes right
 * If it returns anything else, something has gone south
 *
 * Anyone who wants to declare this function as void
 * deserves to get both their arms broken
 */
int main(int argc, const char** argv);
