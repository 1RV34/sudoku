#pragma once

#include "square.hpp"

/**
 * When there's only 1 more potential value in the Square, it'll use this to ask Sudoku to set it's value
 *
 * This way the first found solution will be set first
 */
struct Resolve {
    /**
     * The square asking to be set
     */
    Square *square = nullptr;

    /**
     * The value to be set
     */
    unsigned int value = 0;
};
