#pragma once

#include <array>
#include <iostream>
#include <queue>
#include <fstream>

// Forward declarations
class Sudoku;

#include "group.hpp"
#include "square.hpp"
#include "resolve.hpp"

/**
 * The class hosting the sudoku
 */
class Sudoku {
 private:
    // We declare this method a friend, so it can access our privates
    // Doesn't actually matter what visibility we put it under, but it looked weird without one
    friend std::ostream& operator<<(std::ostream&, const Sudoku&);

 protected:
    /**
     * The squares within the sudoku
     */
    std::array<std::array<Square, 9>, 9> squares;

    /**
     * The columns in the sudoku
     */
    std::array<Group, 9> columns;

    /**
     * The rows in the sudoku
     */
    std::array<Group, 9> rows;

    /**
     * The 3x3 clusters of the sudoku
     */
    std::array<std::array<Group, 3>, 3> clusters;

    /**
     * Makes sure a square's bi-directional connections are correct
     * 
     * During initialization (any kind), we want to make sure the pointers point to the right object
     * 
     * @param x The square's X coordinate
     * @param y The square's Y coordinate
     */
    inline void connectSquare(int x, int y);

    std::queue<Resolve> resolvingQueue;

 public:
    /**
     * The default constructor
     */
    Sudoku();

    /**
     * The copy constructor
     */
    Sudoku(const Sudoku&);

    /**
     * The copy-assignment operator
     */
    Sudoku& operator=(const Sudoku&);

    /**
     * Makes the sudoku initialize itself using a file
     * 
     * @param in
     */
    void init(std::ifstream &in);

    /**
     * Makes the sudoku solve itself
     *
     * It will run only while resolvingQueue is empty
     * If that happens, that can mean two things:
     * * The sudoku is solved, we celebrate
     * * The sudoku reached a state where it cannot narrow itself down any more, guessing game should begin
     */
    void solve();

    /**
     * Sets a given square's value
     *
     * It can be either during initialization or runtime after we started filling the puzzle
     * 
     * @param x   The square's X coord
     * @param y   The square's Y coord
     * @param int The value we want to set
     */
    void setSquareValue(int x, int y, unsigned int);

    /**
     * The squares will use this to ask the sudoku to mark them as solved
     * 
     * @param resolve
     */
    void addToQueue(const Resolve &resolve);

    /**
     * Checks if the sudoku is solved
     */
    bool isSolved() const;

    /**
     * Checks which square is the best candidate for forking the sudoku
     *
     * The best candidate is the square with the least amount of potential, but still having more than 1
     *
     * If no such square exists, we return nullptr
     */
    const Square* getForkCandidate() const;
};
