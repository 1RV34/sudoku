#include "group.hpp"
#include "square.hpp"

void Group::addSquare(Square *square, unsigned int index) {
    squares[index] = square;
}

void Group::setSudoku(Sudoku *sudoku) {
    this->sudoku = sudoku;
}

const Square* Group::contains(unsigned int number) const {
    // Check each square
    for (auto square : squares) {
        if (square->getValue() == number) {
            return square;
        }
    }

    // If we reach this, we haven't found the number
    return nullptr;
}

void Group::setFound(unsigned int number) {
    for (Square *square : squares) {
        square->numberFound(number);
    }
}

void Group::setIndex(unsigned int index) {
    this->index = index;
}

unsigned int Group::getIndex() const {
    return this->index;
}
