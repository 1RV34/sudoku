#include <stdexcept>

#include "square.hpp"
#include "group.hpp"
#include "sudoku.hpp"
#include "resolve.hpp"
// The fmt formatter
#define FMT_HEADER_ONLY
#include "fmt/format.h"

bool Square::canBe(unsigned int number) const {
    return potentialValues.at(number);
}

void Square::setRow(Group *row) {
    this->row = row;
}

void Square::setColumn(Group *column) {
    this->column = column;
}

void Square::setCluster(Group *cluster) {
    this->cluster = cluster;
}

void Square::setSudoku(Sudoku *sudoku) {
    this->sudoku = sudoku;
}

void Square::setValue(unsigned int value) {
    using std::runtime_error;

    if (this->value) {
        throw runtime_error(
            fmt::format(
                "Square {} has already been set before! ({} -> {})",
                getCoords(),
                this->value,
                value
            )
        );
    }

    // We check if the group already contains the given number
    std::string errorWhere = "";
    const Square* errorSquare;
    if ((errorSquare = row->contains(value))) {
        errorWhere = "row";
    } else if ((errorSquare = column->contains(value))) {
        errorWhere = "column";
    } else if ((errorSquare = cluster->contains(value))) {
        errorWhere = "cluster";
    }

    if (errorSquare) {
        throw runtime_error(
            (std::string)fmt::format(
                "Cannot set value of {} to {}, {} already has the same value (same {})",
                getCoords(),
                value,
                errorSquare->getCoords(),
                errorWhere
            )
        );
    }

    // We set the value of the square
    this->value = value;
    resolveRequestSent = true;

    // By setting the value, the value is no longer potential
    potentialCount = 0;
    for (int i = 1; i <= 9; i++) {
        potentialValues[i] = false;
    }

    // We tell everyone else in the groups that this value is taken
    row->setFound(value);
    column->setFound(value);
    cluster->setFound(value);
}

unsigned int Square::getValue() const {
    return value;
}

unsigned int Square::getPotentialCount() const {
    return potentialCount;
}

void Square::numberFound(unsigned int number) {
    potentialValues[number] = false;

    if (resolveRequestSent) {
        return;
    }

    potentialCount = 0;
    unsigned int pot = 0;
    for (int i = 1; i <= 9; i++) {
        if (potentialValues[i]) {
            potentialCount++;
            pot = i;
        }
    }
    if (potentialCount == 1) {
        sudoku->addToQueue({this, pot});
        resolveRequestSent = true;
    }
}

unsigned int Square::getX() const {
    return column->getIndex();
}

unsigned int Square::getY() const {
    return row->getIndex();
}

std::string Square::getCoords() const {
    return fmt::format("{}:{}", getX(), getY());
}
