#include <algorithm>
#include <stdexcept>

#include "sudoku.hpp"

// The fmt formatter
#define FMT_HEADER_ONLY
#include "fmt/format.h"

void Sudoku::connectSquare(int x, int y) {
    // Set the pointers
    Square *square = &squares[x][y];
    Group *row = &rows[y];
    Group *column = &columns[x];
    Group *cluster = &clusters[x / 3][y / 3];

    // Add the square to the groups
    row->addSquare(square, x);
    column->addSquare(square, y);
    cluster->addSquare(square, x % 3 * 3 + y % 3);

    // Add the sudoky to the groups
    row->setSudoku(this);
    column->setSudoku(this);
    cluster->setSudoku(this);

    // Add the groups (and sudoku) to the square
    square->setRow(row);
    square->setColumn(column);
    square->setCluster(cluster);
    square->setSudoku(this);
}

Sudoku::Sudoku() {
    // Set the index of the rows and columns
    for (int i = 0; i < 9; i++) {
        rows[i].setIndex(i + 1);
        columns[i].setIndex(i + 1);
    }

    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            connectSquare(x, y);
        }
    }
}

Sudoku::Sudoku(const Sudoku &sudoku) {
    // Set the index of the rows and columns
    for (int i = 0; i < 9; i++) {
        rows[i].setIndex(i + 1);
        columns[i].setIndex(i + 1);
    }

    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            // Copy the "foreign" square over
            squares[x][y] = sudoku.squares[x][y];

            // Everything else is the same
            connectSquare(x, y);
        }
    }
}

Sudoku& Sudoku::operator=(const Sudoku &sudoku) {
    // Class is already initialized, no need to set row and column index again
    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            // Copy the "foreign" square over
            squares[x][y] = sudoku.squares[x][y];

            // Everything else is the same
            connectSquare(x, y);
        }
    }

    return *this;
}

void Sudoku::solve() {
    while (resolvingQueue.size()) {
        Resolve resolve = resolvingQueue.front();
        resolvingQueue.pop();
        resolve.square->setValue(resolve.value);
    }
}

void Sudoku::setSquareValue(int x, int y, unsigned int value) {
    squares[x][y].setValue(value);
}

void Sudoku::addToQueue(const Resolve &resolve) {
    resolvingQueue.push(resolve);
}

void Sudoku::init(std::ifstream &in) {
    char numbers[][10] = {
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
    };

    for (int y = 0; y < 9; y++) {
        in.getline(numbers[y], 10); // null terminator!
        if (strlen(numbers[y]) < 9) {
            throw std::runtime_error(
                fmt::format(
                    "Expected a 9 characters long line, in line {} we got this instead: '{}' ({} chars long)",
                    y,
                    numbers[y],
                    strlen(numbers[y])
                )
            );
        }
        for (int x = 0; x < 9; x++) {
            if (numbers[y][x] != '0') {
                setSquareValue(y, x, numbers[y][x] - '0');
            }
        }
    }
}

bool Sudoku::isSolved() const {
    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            if (!squares[x][y].getValue()) {
                return false;
            }
        }
    }

    return true;
}

const Square* Sudoku::getForkCandidate() const {
    std::array<const Square*, 9 * 9> squares;
    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            squares[x * 9 + y] = &(this->squares[x][y]);
        }
    }
    std::sort(squares.begin(), squares.end(), [](const Square *a, const Square *b) {
        /**
         * Return true if a should go before b
         */
        return std::abs(a->getPotentialCount() - 2.0) < std::abs(b->getPotentialCount() - 2.0);
    });
    if (squares[0]->getPotentialCount() < 2) {
        return nullptr;
    }
    return squares[0];
}

std::ostream& operator<<(std::ostream& os, const Sudoku& sudoku) {
    for (int x = 0; x < 9; x++) {
        for (int y = 0; y < 9; y++) {
            const Square *square = &sudoku.squares[x][y];
            os << " ";
            if (square->value) {
                os << square->value;
            } else {
                os << " ";
            }
            if (y % 3 == 2 && y != 8) {
                os << " ║";
            }
        }
        os << std::endl;
        if (x % 3 == 2 && x != 8) {
            os << " ══════╬═══════╬══════" << std::endl;
        }
    }
    return os;
}
