#include <fstream>
#include <iostream>
#include "sudoku.hpp"

int main() {
    using std::cout, std::cerr, std::endl, std::ifstream;

    Sudoku sudoku;
    ifstream in("in.txt");
    sudoku.init(in);

    cout << "Initial state:" << endl;
    cout << sudoku;

    cout << "Solving begins..." << endl;
    sudoku.solve();
    cout << "Solving finished" << endl;

    cout << "Current state: " << (sudoku.isSolved() ? "\e[01;32mSolved" : "\e[01;31mNot solved") << "\e[0m" << endl;
    cout << sudoku;

    return 0;
}
